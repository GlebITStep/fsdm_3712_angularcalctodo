import { Component, OnInit } from '@angular/core';
import { BakuBusApiService } from '../baku-bus-api.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-bakubus',
  templateUrl: './bakubus.component.html',
  styleUrls: ['./bakubus.component.scss']
})
export class BakubusComponent implements OnInit {
  lat: number = 40.3798527;
  lng: number = 49.8463909;
  busses: any;
  num: string;
  busNumbers = new Array<string>();
  styles: any;
  icon = {
    url: '../../assets/bus-stop.png',
    scaledSize: {
      width: 30,
      height: 30
    }
  };

  constructor(
    private httpClient: HttpClient,
    private bakuBusApi: BakuBusApiService) { }

  async ngOnInit() {
    this.busses = await this.bakuBusApi.loadBusses();
    this.busNumbers = await this.bakuBusApi.loadBusNumbers();
    this.styles = await this.httpClient.get('../../assets/mapStyle.json').toPromise();

    setInterval(async () => {
      this.busses = await this.bakuBusApi.loadBusses();
    }, 11000);
  }
}