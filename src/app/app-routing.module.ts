import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CalcComponent } from './calc/calc.component';
import { TodoComponent } from './todo/todo.component';
import { BakubusComponent } from './bakubus/bakubus.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { 
    path: '',
    component: HomeComponent
  },
  { 
    path: 'calculator',
    component: CalcComponent
  },
  { 
    path: 'todolist',
    component: TodoComponent
  },
  { 
    path: 'bakubus',
    component: BakubusComponent
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
