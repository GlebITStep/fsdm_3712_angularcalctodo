import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'busNumber'
})
export class BusNumberPipe implements PipeTransform {

  transform(value: any, num: string): any {
    if (num) {
      return value.filter(x => x['@attributes'].DISPLAY_ROUTE_CODE == num); 
    } else {
      return value;
    }
  }

}
