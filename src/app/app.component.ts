import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent {
  title = 'to-do-list';
  image = './assets/cat.png';
  name = '';
  surname = '';
  result = '';

  onButtonClick(): void {
    this.result = `Hello, ${this.name} ${this.surname}!`;
  }
}
