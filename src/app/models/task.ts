export class Task {
	title: string;
	description?: string;
	done: boolean = false;
	priority: Priority;
}

export enum Priority { Low, Medium, High }