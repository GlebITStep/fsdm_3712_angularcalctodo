import { Component } from '@angular/core';

@Component({
  selector: 'app-calc',
  templateUrl: './calc.component.html',
  styleUrls: ['./calc.component.scss']
})
export class CalcComponent {
  num1: number;
  num2: number;
  operation: string;
  result: number;

  onCalc() {
    if (this.operation == '+')
      this.result = this.num1 + this.num2;
    else if (this.operation == '-')
      this.result = this.num1 - this.num2;
    else if (this.operation == '*')
      this.result = this.num1 * this.num2;
    else if (this.operation == '/')
      this.result = this.num1 / this.num2; 
  }
}
