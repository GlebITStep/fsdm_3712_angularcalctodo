import { Component, OnInit } from '@angular/core';
import { Task, Priority } from '../models/task';
import { tick } from '@angular/core/src/render3';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  newTask = new Task();
  tasks = new Array<Task>();
  editState = false;
 
  ngOnInit() {
    this.tasks = this.getTasks(); 
  }

  onTaskAdd(): void {
    this.tasks.push(this.newTask);
    this.newTask = new Task(); 
    this.saveChanges();
  }

  onTaskDelete(task: Task): void {
    this.tasks = this.tasks.filter(x => x != task);
    this.saveChanges();
  }

  onTaskClick(task: Task): void {
    task.done = !task.done;
    this.saveChanges();
  }

  onTaskEdit(task: Task): void {
    this.newTask = task;
    this.editState = true;
  }

  editStateOff(): void {
    this.editState = false;
    this.newTask = new Task();
    this.saveChanges();
  }

  saveChanges(): void {
    let json = JSON.stringify(this.tasks);
    localStorage.setItem('taskList', json);
  }

  getTasks(): Array<Task> {
    let json = localStorage.getItem('taskList');
    if (json) {
      return JSON.parse(json)
    } else {
      return new Array<Task>();
    } 
  }
}







    // this.tasks.push({ 
    //   title: 'one', 
    //   description: 'lorem ipsum',
    //   done: false,
    //   priority: Priority.Medium
    // });

    // this.tasks.push({ 
    //   title: 'two', 
    //   description: 'dolor sit amet',
    //   done: true,
    //   priority: Priority.Low
    // });

    // this.tasks.push({ 
    //   title: 'three', 
    //   description: 'hello world',
    //   done: false,
    //   priority: Priority.High
    // });

    // let json = JSON.stringify(this.tasks);
    // localStorage.setItem('taskList', json); 